#!/bin/bash

INTYBASIC_LIB_PATH=$(dirname `which intybasic`)
INTYBAS=""
CODE=""
CODE_PATH=""
FILENAME=""

assert_filename(){
  export CODE="$1"
  stat_file $CODE
  export CODE_PATH=$(dirname "$1")
  last=${CODE##*/}
  export FILENAME=${last%%.bas}
}

build_map_symbol(){
  $INTYBAS $CODE_PATH/$FILENAME.bas $CODE_PATH/$FILENAME.asm $INTYBASIC_LIB_PATH

  echo "Building source map and symbol file for [$FILENAME"]
  as1600 --sym-file $FILENAME.sym --src-map $FILENAME.map $CODE_PATH/$FILENAME.asm

  echo "Compiling rom/bin from [$FILENAME.asm]"
  as1600 -o $CODE_PATH/$FILENAME $CODE_PATH/$FILENAME.asm
}

check_err(){ error_state=$(echo $?)
if [[ "$error_state" != "0" ]];then
    echo $1
    exit 1
fi
}

cleanup(){
  rm -f \
    $CODE_PATH/$FILENAME.rom \
    $CODE_PATH/$FILENAME.bin \
    $CODE_PATH/$FILENAME.cfg \
    $CODE_PATH/$FILENAME.asm \
    $CODE_PATH/$FILENAME.sym \
    $CODE_PATH/$FILENAME.map
}

compile(){
  $INTYBAS $CODE_PATH/$FILENAME.bas $CODE_PATH/$FILENAME.asm $INTYBASIC_LIB_PATH
  as1600 -o $CODE_PATH/$FILENAME $CODE_PATH/$FILENAME.asm
}

print_usage(){
    echo "Usage: "$0" [INTV_BASIC_FILE].bas"
    exit 1
}

jzintv_debug(){
  jzintv -z3 \
    -d \
    --sym-file $CODE_PATH/$FILENAME.sym \
    --src-map $CODE_PATH/$FILENAME.map \
    $CODE_PATH/$FILENAME.rom
}

jzintv_run(){
  jzintv -z3 $CODE_PATH/$FILENAME.rom
}

set_os(){
  os=$(uname -s)
  if [ "$os" = "Darwin" ]; then
    export INTYBAS=intybasic
  else
    export INTYBAS=intybasic_linux
  fi
}

test_deps()
{
  for dependency in $INTYBAS as1600
  do
    # echo -n "Testing for dependency - "$dependency""
    command -v $dependency >/dev/null 2>&1 || {
      echo >&2 "'$dependency' not found. unable to continue."
      exit 1;
    }
    # echo ": pass"
  done
}

stat_file(){
  code="$1"
  # Test to see file exists
  stat "$code" > /dev/null 2>&1
  check_err "Err: No file "$code""
}



set_os
test_deps

# Main processing loop
while getopts :c:s:d:e:h option
do
  case "${option}" in
    e)
        INTYBAS_FILE=$OPTARG
        assert_filename $INTYBAS_FILE
        compile
        jzintv_run
        exit;;
    c)
        INTYBAS_FILE=$OPTARG
        assert_filename $INTYBAS_FILE
        cleanup
        exit;;
    d)
        INTYBAS_FILE=$OPTARG
        assert_filename $INTYBAS_FILE
        build_map_symbol
        jzintv_debug
        exit;;
    h)
        print_usage
        exit;;
    s)
        # Generate source map and symbol file
        INTYBAS_FILE=$OPTARG
        assert_filename $INTYBAS_FILE
        build_map_symbol
        exit;;
    *)
        echo $prog: illegal option -- ${OPTARG}
        print_usage
        exit;;
  esac
done


# Default behavior - compile FILE.bas and exit
if [ "$#" -eq 1 ]; then
    INTYBAS_FILE="$1"
    assert_filename $INTYBAS_FILE
    compile
    exit
fi

if [[ -z "$INTYBAS_FILE" ]];
  then
    echo "Err: Missing a required [FILE.bas] required to invoke compiler"
    print_usage
fi
