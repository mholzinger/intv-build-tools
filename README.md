# Intellivision™ Build Tools

Project name: intv-build-tools

_[ UPDATED - 2020-12-01: Updates to dependencies SDL2 for jzintv, 64 bit binaries and OSX Big Sur support! ]_

_[TODO: Add Linux and WinX64 instructions]_

#### Summary:

Helper scripts to build Intellivision binaries from IntyBASIC. Guide included in readme.

---

#### Program Usage:

#### Compile ROM from INTYBasic source

#### ```build_intv.sh [path_to_intybasic_source].bas```

Invokes intybasic to generate assembly and as1600 to generate a compiled rom and bin file.

---

#### Generate source map and symbol file for debugging with jzintv

#### ```build_intv.sh -s [path_to_intybasic_source].bas```

Command will output `FILE.sym, FILE.map, FILE.rom, FILE.bin`

---

#### Clean all compiled files, just leave the source

#### ```build_intv.sh -c [path_to_intybasic_source].bas```

Will remove any `FILE.sym, FILE.map, FILE.rom, FILE.bin` while leaving `FILE.bas` intact

---

#### Compile rom from source and invoke jzintv

#### ```build_intv.sh -e [path_to_intybasic_source].bas```

Generates a compiled rom and bin file and calls jzintv to execute the compiled rom

---

#### Compile rom from source and invoke jzintv (debug symbol mode)

#### ```build_intv.sh -d [path_to_intybasic_source].bas```

Generates compiled rom/bin and symbol map and symbol file. Calls jzintv to execute the compiled source in debug stepping mode

---

#### Citing sources

This guide is a summary posts at the atariage forums.

_[IntyBASIC compiler v1.4.2: reloaded with new features](https://atariage.com/forums/topic/286953-intybasic-compiler-v142-reloaded-with-new-features/)_

---

##### 1) Download IntyBasic compiler tools

- link: [Linux/OS X/Windows - intybasic-compiler-v1.4.2.zip](https://atariage.com/forums/applications/core/interface/file/attachment.php?id=759739)

(https://atariage.com/forums/applications/core/interface/file/attachment.php?id=759739)

- Unzip tools to home path and create link to extracted folder as `intybasic`

```
unzip ~/Downloads/intybasic_compiler_v1.4.2.zip -d ~/intybasic_compiler_v1.4.2
ln -s ~/intybasic_compiler_v1.4.2/ ~/intybasic
```

---

##### 2) Download `jzintv` and the associated `as1600` compiler bundle

Download the jzintv bundle from spatula-city.org

_[Note: Latest updates use newer SDL2 framework!]_

**Linux**

```
wget http://spatula-city.org/~im14u2c/intv/dl/jzintv-20200712-linux-x86-64-sdl2.zip
unzip -d ~/ jzintv-20200712-linux-x86-64-sdl2.zip
```

- Create link to extracted folder as `jzintv`

```
ln -s ~/jzintv-20200712-linux-x86-64-sdl2 ~/jzintv
```

- x86_64 - [jzintv-20200712-linux-x86-64-sdl2.zip](http://spatula-city.org/~im14u2c/intv/dl/jzintv-20200712-linux-x86-64-sdl2.zip)


- ARM/RPi - [jzintv-20200712-linux-rpi-sdl2.zip](http://spatula-city.org/~im14u2c/intv/dl/jzintv-20200712-linux-rpi-sdl2.zip)

---

**Mac OSX**

```
wget http://spatula-city.org/~im14u2c/intv/dl/jzintv-20200712-osx-sdl2.zip
unzip -d ~/ jzintv-20200712-osx-sdl2.zip
```

- Create link to extracted folder as `jzintv`

```
ln -s ~/jzintv-20200712-osx-sdl2 ~/jzintv
```

- OSX - [jzintv-20200712-osx-sdl2.zip](http://spatula-city.org/~im14u2c/intv/dl/jzintv-20200712-osx-sdl2.zip)

---

##### 3) jzintv

**Dependencies needed to execute `jzintv` (OSX/Linux)**

**Linux**

Compile and link SDL2 library - [http://libsdl.org/download-2.0.php](http://libsdl.org/download-2.0.php)

_NOTE: DEPRICATED SDL1 library info below_

Linux Dependency SDL 1.2.15:

_Instructions captured from original post at linuxquestions.org_ _[[SOLVED] Can't compile sdl](https://www.linuxquestions.org/questions/linux-from-scratch-13/can%27t-compile-sdl-4175469154/)_

```
wget http://www.libsdl.org/release/SDL-1.2.15.tar.gz
wget http://hg.libsdl.org/SDL/raw-rev/91ad7b43317a -O sdl.patch
tar -xvf SDL-1.2.15.tar.gz
cd SDL-1.2.15/
patch -Np1 -i ../sdl.patch
./configure
make
sudo make install
```

---

**Mac OSX**

Install guide and post from atariage forums: [jzintv osx](http://atariage.com/forums/topic/272002-jzintv-osx/)

OSX Dependency: simple Direct Media Layer

SDL 2.0.12: _[SDL version 2.0.12 (stable)
](http://libsdl.org/release/SDL2-2.0.12.dmg)_

---

##### 4) Export paths to SHELL profile

**Mac OSX**

```
echo 'export PATH=$PATH:$HOME/intybasic:$HOME/jzintv/bin' >> ~/.profile
source ~/.profile
```

**Linux**

```
echo 'export PATH=$PATH:$HOME/intybasic:$HOME/jzintv/bin' >> ~/.bashrc
source ~/.bashrc
```

---

##### 5) Intellivision BIOS needed to execute compiled roms (jzintv)

`exec.bin`

CRC32 = CBCE86F7
SHA1 = 5a65b922b562cb1f57dab51b73151283f0e20c7a
MD5 = 62e761035cb657903761800f4437b8af

`grom.bin`

CRC32 = 683A4158
SHA1 = f9608bb4ad1cfe3640d02844c7ad8e0bcd974917
MD5 = 0cd5946c6473e42e8e4c2137785e427f

`ecs.bin`

CRC32 = EA790A06
SHA1 = b7ccb38b881d7f8426cd6f1f8a7aabbd31784fc5
MD5 = 2e72a9a2b897d330a35c8b07a6146c52
